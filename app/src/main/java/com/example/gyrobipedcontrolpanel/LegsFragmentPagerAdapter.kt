package com.example.gyrobipedcontrolpanel

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.gyrobipedcontrolpanel.fragments.*

class LegsFragmentPagerAdapter(fragmentManager: FragmentManager, private var tabCount: Int): FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment? {
        return when (position) {
            0 -> MotionControlFragment()
            1 -> InverseKinematicsFragment()
            else -> EmptyFragment()
        }
    }

    override fun getCount(): Int {
        return tabCount
    }
}