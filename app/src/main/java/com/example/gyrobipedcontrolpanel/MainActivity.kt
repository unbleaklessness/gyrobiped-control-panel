package com.example.gyrobipedcontrolpanel

import android.annotation.TargetApi
import android.content.pm.ActivityInfo
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.example.gyrobipedcontrolpanel.networking.OutputPacketFields
import com.example.gyrobipedcontrolpanel.networking.UDPClient
import com.example.gyrobipedcontrolpanel.stateful.OnContainerStateChangeListener
import com.example.gyrobipedcontrolpanel.views.GyrobipedModel
import java.net.DatagramPacket
import java.nio.ByteBuffer
import java.nio.ByteOrder

class MainActivity
    : AppCompatActivity()
    , OnContainerStateChangeListener {

    companion object {
        val TAG = "MainActivity"
    }

    inner class ConnectAndReceiveTask: AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg params: Unit?) {
            udpClient = UDPClient(serverIP, serverPort, onMessageReceiveListener)
            udpClient?.run()
        }
    }

    inner class SendTask: AsyncTask<Unit, Unit, Unit>() {

        private var lastTime: Long = 0

        override fun doInBackground(vararg params: Unit?) {

            udpRunning = true

            while (udpRunning) {
                val time = System.currentTimeMillis()

                if (time - lastTime >= 20) {
                    lastTime = time

                    udpClient?.sendBytes(getPacket())
                }
            }
        }
    }

    inner class OnMessageReceiveListener: UDPClient.OnMessageReceiveListener {

        private var lastTime: Long = 0

        override fun messageReceived(packet: DatagramPacket) {
            val buffer = ByteBuffer.wrap(packet.data).order(ByteOrder.LITTLE_ENDIAN)

            gyrobipedModel?.backGyroAngle = buffer.float
            gyrobipedModel?.backGyroVelocity = buffer.float
            gyrobipedModel?.frontGyroAngle = buffer.float
            gyrobipedModel?.frontGyroVelocity = buffer.float
            gyrobipedModel?.leftGyroAngle = buffer.float
            gyrobipedModel?.leftGyroVelocity = buffer.float
            gyrobipedModel?.rightGyroAngle = buffer.float
            gyrobipedModel?.rightGyroVelocity = buffer.float
            gyrobipedModel?.rightThighAngle = buffer.float
            gyrobipedModel?.rightThighVelocity = buffer.float
            gyrobipedModel?.rightKneeAngle = buffer.float
            gyrobipedModel?.rightKneeVelocity = buffer.float
            gyrobipedModel?.leftThighAngle = buffer.float
            gyrobipedModel?.leftThighVelocity = buffer.float
            gyrobipedModel?.leftKneeAngle = buffer.float
            gyrobipedModel?.leftKneeVelocity = buffer.float
            gyrobipedModel?.pitchPosition = buffer.float
            gyrobipedModel?.rollPosition = buffer.float
            gyrobipedModel?.yawPosition = buffer.float
            gyrobipedModel?.pitchVelocity = buffer.float
            gyrobipedModel?.rollVelocity = buffer.float
            gyrobipedModel?.yawVelocity = buffer.float
            gyrobipedModel?.invalidate()

            val time = System.currentTimeMillis()

            if (time - lastTime > 100) {

                lastTime = time

                gyrosInformation?.text = gyrobipedModel?.gyrosInformation()
                legsInformation?.text = gyrobipedModel?.legsInformation()
                gyroscopeInformation?.text = gyrobipedModel?.gyroscopeInformation()
            }
        }
    }

    // Legs layout:
    private var legsTabLayout: TabLayout? = null
    private var legsViewPager: ViewPager? = null
    private var legsFragmentPagerAdapter: LegsFragmentPagerAdapter? = null

    // Gyro layout:
    private var gyrosTabLayout: TabLayout? = null
    private var gyrosViewPager: ViewPager? = null
    private var gyrosFragmentPagerAdapter: GyrosFragmentPagerAdapter? = null

    // Gyrobiped model:
    private var gyrobipedModel: GyrobipedModel? = null

    // UDP:
    private val serverIP: String = "192.168.1.2"
    private val serverPort: Int = 2007
    private var udpClient: UDPClient? = null
    private var udpRunning = false
    private var onMessageReceiveListener: OnMessageReceiveListener? = null

    // Information text views:
    private var gyrosInformation: TextView? = null
    private var legsInformation: TextView? = null
    private var gyroscopeInformation: TextView? = null

    private var globalState: HashMap<OutputPacketFields, Byte> = initializeGlobalState()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE

        onMessageReceiveListener = OnMessageReceiveListener()

        findViews()
        setupTabs()

        connect()
    }

    override fun onPause() {
        super.onPause()
        disconnect()
    }

    override fun onResume() {
        super.onResume()
        connect()
    }

    override fun containerStateChanged(state: HashMap<OutputPacketFields, Byte>) {
        globalState.putAll(state)
    }

    private fun getPacket(): ByteArray {
        return globalState.toSortedMap().values.toByteArray()
    }

    private fun initializeGlobalState(): HashMap<OutputPacketFields, Byte> {
        val result = HashMap<OutputPacketFields, Byte>()
        for (field in OutputPacketFields.values()) {
            result[field] = 0
        }
        return result
    }

    private fun setupTabs() {

        legsTabLayout?.let {
            it.addTab(it.newTab().setText("Motion control"))
            it.addTab(it.newTab().setText("Inverse kinematics"))

            legsFragmentPagerAdapter = LegsFragmentPagerAdapter(supportFragmentManager, it.tabCount)
            legsViewPager?.adapter = legsFragmentPagerAdapter
        }

        gyrosTabLayout?.let {
            it.addTab(it.newTab().setText("Full gyro control"))
            it.addTab(it.newTab().setText("Demo gyro control"))

            gyrosFragmentPagerAdapter = GyrosFragmentPagerAdapter(supportFragmentManager, it.tabCount)
            gyrosViewPager?.adapter = gyrosFragmentPagerAdapter
        }

        legsViewPager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(legsTabLayout))
        legsTabLayout?.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                legsViewPager?.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        gyrosViewPager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(gyrosTabLayout))
        gyrosTabLayout?.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                gyrosViewPager?.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
    }

    private fun findViews() {
        gyrobipedModel = findViewById(R.id.gyrobiped_model)
        legsViewPager = findViewById(R.id.legs_view_pager)
        gyrosViewPager = findViewById(R.id.gyros_view_pager)
        legsTabLayout = findViewById(R.id.legs_tab_layout)
        gyrosTabLayout = findViewById(R.id.gyros_tab_layout)
        gyrosInformation = findViewById(R.id.gyrosInformation)
        legsInformation = findViewById(R.id.legsInformation)
        gyroscopeInformation = findViewById(R.id.gyroscopeInformation)
    }

    private fun disconnect() {
        udpRunning = false

        udpClient?.stop()
        udpClient = null
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    fun <Params> startMyTask(asyncTask: AsyncTask<Params, *, *>, vararg params: Params) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            asyncTask.executeOnExecutor(
                AsyncTask.THREAD_POOL_EXECUTOR,
                *params)
        } else {
            asyncTask.execute(*params)
        }
    }

    private fun connect() {
        startMyTask(ConnectAndReceiveTask())
        startMyTask(SendTask())
    }
}
