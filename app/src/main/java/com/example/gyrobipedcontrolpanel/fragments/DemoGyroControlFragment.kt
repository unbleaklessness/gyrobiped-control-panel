package com.example.gyrobipedcontrolpanel.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gyrobipedcontrolpanel.networking.OutputPacketFields
import com.example.gyrobipedcontrolpanel.R
import com.example.gyrobipedcontrolpanel.stateful.OnContainerStateChangeListener
import com.example.gyrobipedcontrolpanel.stateful.StatefulContainer
import com.example.gyrobipedcontrolpanel.views.ToggleButton

class DemoGyroControlFragment: Fragment(), StatefulContainer {

    private var masterButton: ToggleButton? = null
    private var zeroButton: ToggleButton? = null
    private var yawButton: ToggleButton? = null

    private var onContainerStateChangeListener: OnContainerStateChangeListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            onContainerStateChangeListener = it as? OnContainerStateChangeListener
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val inflatedView = inflater.inflate(R.layout.fragment_demo_gyro_control, container, false)

        inflatedView?.let {
            masterButton = it.findViewById(R.id.masterButton)
            zeroButton = it.findViewById(R.id.zeroButton)
            yawButton = it.findViewById(R.id.yawButton)
        }

        setupOnClickListeners()

        return inflatedView
    }

    private fun setupOnClickListeners() {

        masterButton?.setOnClickListener {
            zeroButton?.toggleOff()
        }
        zeroButton?.setOnClickListener {
            masterButton?.toggleOff()
        }

        listOf(masterButton, zeroButton, yawButton).map {
            it?.setOnClickListener {
                onContainerStateChangeListener?.containerStateChanged(getState())
            }
        }
    }

    override fun getState(): HashMap<OutputPacketFields, Byte> {
        return hashMapOf(
            Pair(OutputPacketFields.GyrosMaster, masterButton?.getState() ?: 0),
            Pair(OutputPacketFields.GyrosZero, zeroButton?.getState() ?: 0),
            Pair(OutputPacketFields.GyrosYaw, yawButton?.getState() ?: 0)
        )
    }
}