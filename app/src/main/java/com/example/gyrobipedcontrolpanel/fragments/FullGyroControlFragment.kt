package com.example.gyrobipedcontrolpanel.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gyrobipedcontrolpanel.R
import com.example.gyrobipedcontrolpanel.views.MyImageButton
import com.example.gyrobipedcontrolpanel.views.ToggleButton

class FullGyroControlFragment: Fragment() {

    private var masterButton: ToggleButton? = null
    private var zeroButton: ToggleButton? = null
    private var pitchZeroButton: ToggleButton? = null
    private var pitchEnableButton: ToggleButton? = null
    private var pitchUpButton: MyImageButton? = null
    private var pitchDownButton: MyImageButton? = null
    private var rollZeroButton: ToggleButton? = null
    private var rollEnableButton: ToggleButton? = null
    private var rollUpButton: MyImageButton? = null
    private var rollDownButton: MyImageButton? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val inflatedView = inflater.inflate(R.layout.fragment_full_gyro_control, container, false)

        inflatedView?.let {
            masterButton = it.findViewById(R.id.masterButton)
            zeroButton = it.findViewById(R.id.globalZeroButton)
            pitchZeroButton = it.findViewById(R.id.pitchZeroButton)
            pitchEnableButton = it.findViewById(R.id.pitchEnableButton)
            pitchUpButton = it.findViewById(R.id.pitchUpButton)
            pitchDownButton = it.findViewById(R.id.pitchDownButton)
            rollZeroButton = it.findViewById(R.id.rollZeroButton)
            rollEnableButton = it.findViewById(R.id.rollEnableButton)
            rollUpButton = it.findViewById(R.id.rollUpButton)
            rollDownButton = it.findViewById(R.id.rollDownButton)
        }

        setupOnClickListeners()

        return inflatedView
    }

    private fun setupOnClickListeners() {

        listOf(masterButton, zeroButton).map {
            it?.setOnClickListener {
                pitchZeroButton?.toggleOff()
                pitchEnableButton?.toggleOff()
                rollZeroButton?.toggleOff()
                rollEnableButton?.toggleOff()
            }
        }

        masterButton?.setOnClickListener {
            zeroButton?.toggleOff()
        }
        zeroButton?.setOnClickListener {
            masterButton?.toggleOff()
        }

        pitchZeroButton?.setOnClickListener {
            pitchEnableButton?.toggleOff()
            masterButton?.toggleOff()
            zeroButton?.toggleOff()
        }
        pitchEnableButton?.setOnClickListener {
            pitchZeroButton?.toggleOff()
            masterButton?.toggleOff()
            zeroButton?.toggleOff()
        }

        rollZeroButton?.setOnClickListener {
            rollEnableButton?.toggleOff()
            masterButton?.toggleOff()
            zeroButton?.toggleOff()
        }
        rollEnableButton?.setOnClickListener {
            rollZeroButton?.toggleOff()
            masterButton?.toggleOff()
            zeroButton?.toggleOff()
        }
    }
}