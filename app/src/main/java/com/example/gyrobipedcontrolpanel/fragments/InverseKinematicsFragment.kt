package com.example.gyrobipedcontrolpanel.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import com.example.gyrobipedcontrolpanel.R
import com.example.gyrobipedcontrolpanel.networking.OutputPacketFields
import com.example.gyrobipedcontrolpanel.stateful.OnContainerStateChangeListener
import com.example.gyrobipedcontrolpanel.stateful.StatefulContainer
import com.example.gyrobipedcontrolpanel.views.MySeekBar
import com.example.gyrobipedcontrolpanel.views.ToggleButton

class InverseKinematicsFragment: Fragment(), StatefulContainer {

    var stopButton: ToggleButton? = null
    var zeroButton: ToggleButton? = null
    var leftVerticalSeekBar: MySeekBar? = null
    var rightVerticalSeekBar: MySeekBar? = null
    var leftHorizontalSeekBar: MySeekBar? = null
    var rightHorizontalSeekBar: MySeekBar? = null

    private var onContainerStateChangeListener: OnContainerStateChangeListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            onContainerStateChangeListener = it as? OnContainerStateChangeListener
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val inflatedView = inflater.inflate(R.layout.fragment_inverse_kinematics, container, false)

        inflatedView?.let {
            stopButton = it.findViewById(R.id.stopButton)
            zeroButton = it.findViewById(R.id.zeroButton)
            leftVerticalSeekBar = it.findViewById(R.id.leftVerticalSeekBar)
            rightVerticalSeekBar = it.findViewById(R.id.rightVerticalSeekBar)
            leftHorizontalSeekBar = it.findViewById(R.id.leftHorizontalSeekBar)
            rightHorizontalSeekBar = it.findViewById(R.id.rightHorizontalSeekBar)
        }

        setupOnClickListeners()

        return inflatedView
    }

    private fun setupOnClickListeners() {

        stopButton?.setOnClickListener {
            zeroButton?.toggleOff()
        }
        zeroButton?.setOnClickListener {
            stopButton?.toggleOff()
        }

        listOf(stopButton, zeroButton).map {
            (it as? View)?.setOnClickListener {
                onContainerStateChangeListener?.containerStateChanged(getState())
            }
        }

        val onSeekBarChangeListener = object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, position: Int, flag: Boolean) {
                onContainerStateChangeListener?.containerStateChanged(getState())
            }
            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        }

        listOf(leftVerticalSeekBar, rightVerticalSeekBar, leftHorizontalSeekBar, rightHorizontalSeekBar).map {
            (it as? SeekBar)?.setOnSeekBarChangeListener(onSeekBarChangeListener)
        }
    }

    override fun getState(): HashMap<OutputPacketFields, Byte> {
        return hashMapOf()
    }
}