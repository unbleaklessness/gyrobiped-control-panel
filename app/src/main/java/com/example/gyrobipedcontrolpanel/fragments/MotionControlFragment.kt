package com.example.gyrobipedcontrolpanel.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gyrobipedcontrolpanel.networking.OutputPacketFields
import com.example.gyrobipedcontrolpanel.R
import com.example.gyrobipedcontrolpanel.stateful.OnContainerStateChangeListener
import com.example.gyrobipedcontrolpanel.stateful.StatefulContainer
import com.example.gyrobipedcontrolpanel.views.MyImageButton
import com.example.gyrobipedcontrolpanel.views.ToggleButton

class MotionControlFragment: Fragment(), StatefulContainer {

    private var stopButton: ToggleButton? = null
    private var zeroButton: ToggleButton? = null
    private var upButton: MyImageButton? = null
    private var circleButton: MyImageButton? = null
    private var downButton: MyImageButton? = null
    private var leftButton: MyImageButton? = null
    private var rightButton: MyImageButton? = null

    private var onContainerStateChangeListener: OnContainerStateChangeListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        context?.let {
            onContainerStateChangeListener = it as? OnContainerStateChangeListener
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val inflatedView = inflater.inflate(R.layout.fragment_motion_control, container, false)

        inflatedView?.let {
            stopButton = it.findViewById(R.id.stopButton)
            zeroButton = it.findViewById(R.id.zeroButton)
            upButton = it.findViewById(R.id.upButton)
            circleButton = it.findViewById(R.id.circleButton)
            downButton = it.findViewById(R.id.downButton)
            leftButton = it.findViewById(R.id.leftButton)
            rightButton = it.findViewById(R.id.rightButton)
        }

        setupOnClickListeners()

        return inflatedView
    }

    private fun setupOnClickListeners() {

        listOf(upButton, circleButton, downButton).map {
            it?.setOnClickListener {
                zeroButton?.toggleOff()
                stopButton?.toggleOff()
            }
        }

        upButton?.setOnClickListener {
            circleButton?.toggleOff()
            downButton?.toggleOff()
            rightButton?.toggleOff()
            leftButton?.toggleOff()
        }
        circleButton?.setOnClickListener {
            upButton?.toggleOff()
            downButton?.toggleOff()
            rightButton?.toggleOff()
            leftButton?.toggleOff()
        }
        downButton?.setOnClickListener {
            circleButton?.toggleOff()
            upButton?.toggleOff()
            rightButton?.toggleOff()
            leftButton?.toggleOff()
        }

        zeroButton?.setOnClickListener {
            stopButton?.toggleOff()
            rightButton?.toggleOff()
            leftButton?.toggleOff()
        }
        stopButton?.setOnClickListener {
            zeroButton?.toggleOff()
            rightButton?.toggleOff()
            leftButton?.toggleOff()
        }

        leftButton?.setOnClickListener {
            rightButton?.toggleOff()
        }
        rightButton?.setOnClickListener {
            leftButton?.toggleOff()
        }

        listOf(zeroButton, stopButton).map {
            it?.setOnClickListener {
                circleButton?.toggleOff()
                downButton?.toggleOff()
                upButton?.toggleOff()
            }
        }

        listOf(stopButton, zeroButton, upButton, circleButton, downButton, leftButton, rightButton).map {
            (it as? View)?.setOnClickListener {
                onContainerStateChangeListener?.containerStateChanged(getState())
            }
        }
    }

    override fun getState(): HashMap<OutputPacketFields, Byte> {
        return hashMapOf(
            Pair(OutputPacketFields.LegsZero, zeroButton?.getState() ?: 0),
            Pair(OutputPacketFields.LegsWalkForward, upButton?.getState() ?: 0),
            Pair(OutputPacketFields.LegsMarch, circleButton?.getState() ?: 0),
            Pair(OutputPacketFields.TurnLeft, leftButton?.getState() ?: 0),
            Pair(OutputPacketFields.TurnRight, rightButton?.getState() ?: 0)
        )
    }
}