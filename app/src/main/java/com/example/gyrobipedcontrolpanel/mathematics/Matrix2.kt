package com.example.gyrobipedcontrolpanel.mathematics

import kotlin.math.cos
import kotlin.math.sin

class Matrix2(
    var ax: Float = 0.0f, var bx: Float = 0.0f,
    var ay: Float = 0.0f, var by: Float = 0.0f) {

    companion object {
        fun rotation(angle: Float): Matrix2 {
            return Matrix2(cos(angle), -sin(angle), sin(angle), cos(angle))
        }
    }

    fun multiply(other: Matrix2): Matrix2 {
        return Matrix2(
            ax * other.ax + bx * other.ay, ax * other.bx + bx * other.by,
            ay * other.ax + by * other.ay, ay * other.bx + by * other.by)
    }

    fun multiply(n: Float): Matrix2 {
        return Matrix2(ax * n, bx * n, ay * n, by * n)
    }

    fun multiply(v: Vector2): Vector2 {
        return Vector2(v.x * ax + v.y * bx, v.x * ay + v.y * by)
    }

    fun add(n: Float): Matrix2 {
        return Matrix2(ax + n, bx + n, ay + n, by + n)
    }
    
    fun add(other: Matrix2): Matrix2 {
        return Matrix2(ax + other.ax, bx + other.bx, ay + other.ay, by + other.by)
    }

    fun subtract(n: Float): Matrix2 {
        return Matrix2(ax - n, bx - n, ay - n, by - n)
    }

    fun subtract(other: Matrix2): Matrix2 {
        return Matrix2(ax - other.ax, bx - other.bx, ay - other.ay, by - other.by)
    }
}