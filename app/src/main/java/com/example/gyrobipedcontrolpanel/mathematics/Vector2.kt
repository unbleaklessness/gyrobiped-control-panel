package com.example.gyrobipedcontrolpanel.mathematics

class Vector2(var x: Float = 0.0f, var y: Float = 0.0f) {

    fun add(other: Vector2): Vector2 {
        return Vector2(x + other.x, y + other.y)
    }

    fun add(n: Float): Vector2 {
        return Vector2(x + n, y + n)
    }

    fun subtract(other: Vector2): Vector2 {
        return Vector2(x - other.x, y - other.y)
    }

    fun subtract(n: Float): Vector2 {
        return Vector2(x - n, y - n)
    }


    fun multiply(other: Vector2): Vector2 {
        return Vector2(x * other.x, y * other.y)
    }

    fun multiply(n: Float): Vector2 {
        return Vector2(x * n, y * n)
    }

    fun negative(): Vector2 {
        return Vector2(-x, -y)
    }
}