package com.example.gyrobipedcontrolpanel.networking

enum class OutputPacketFields {
    GyrosMaster,
    GyrosZero,
    LegsZero,
    LegsMarch,
    LegsWalkForward,
    TurnLeft,
    TurnRight,
    GyrosYaw,
}