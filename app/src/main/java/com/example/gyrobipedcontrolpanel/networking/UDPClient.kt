package com.example.gyrobipedcontrolpanel.networking

import java.lang.Exception
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress

class UDPClient(serverIP: String, private val serverPort: Int, private var listener: OnMessageReceiveListener?) {

    companion object {
        val TAG = "UDPClient"
    }

    interface OnMessageReceiveListener {
        fun messageReceived(packet: DatagramPacket)
    }

    private var running = false
    private var socket: DatagramSocket? = null
    private var serverAddress: InetAddress = InetAddress.getByName(serverIP)

    fun run() {
        running = true

        while (running) {
            try {
                socket = DatagramSocket()

                while (running) {
                    val receiveData = ByteArray(88)
                    val receivePacket = DatagramPacket(receiveData, receiveData.size)
                    socket?.soTimeout = 500
                    socket?.receive(receivePacket)
                    listener?.messageReceived(receivePacket)
                }
            } catch (_: Exception) {
            } finally {
                if (socket?.isClosed == false) {
                    socket?.close()
                }
                socket = null
            }
        }

        listener = null
    }

    fun stop() {
        running = false
    }

    fun sendBytes(bytes: ByteArray) {
        if (running) {
            try {
                Thread(Runnable {
                    try {
                        socket?.send(DatagramPacket(bytes, bytes.size, serverAddress, serverPort))
                    } catch (_: Exception) {}
                }).start()
            } catch (_: Exception) {}
        }
    }
}