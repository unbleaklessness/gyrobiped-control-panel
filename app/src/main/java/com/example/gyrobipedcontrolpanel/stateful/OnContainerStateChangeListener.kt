package com.example.gyrobipedcontrolpanel.stateful

import com.example.gyrobipedcontrolpanel.networking.OutputPacketFields

interface OnContainerStateChangeListener {
    fun containerStateChanged(state: HashMap<OutputPacketFields, Byte>)
}