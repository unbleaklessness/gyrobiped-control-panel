package com.example.gyrobipedcontrolpanel.stateful

interface OnElementStateChangeListener {
    fun elementStateChanged(state: Byte)
}