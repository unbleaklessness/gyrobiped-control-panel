package com.example.gyrobipedcontrolpanel.stateful

import com.example.gyrobipedcontrolpanel.networking.OutputPacketFields

interface StatefulContainer {
    fun getState(): HashMap<OutputPacketFields, Byte>
}