package com.example.gyrobipedcontrolpanel.stateful

interface StatefulElement {
    fun getState(): Byte
}