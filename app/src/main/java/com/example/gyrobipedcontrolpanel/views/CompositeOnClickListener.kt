package com.example.gyrobipedcontrolpanel.views

import android.view.View

class CompositeOnClickListener: View.OnClickListener {

    private var listeners: MutableList<View.OnClickListener> = ArrayList()

    fun addOnClickListener(listener: View.OnClickListener) {
        listeners.add(listener)
    }

    override fun onClick(v: View) {
        for (listener in listeners) {
            listener.onClick(v)
        }
    }
}