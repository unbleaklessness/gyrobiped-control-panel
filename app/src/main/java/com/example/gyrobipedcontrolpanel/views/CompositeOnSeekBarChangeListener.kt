package com.example.gyrobipedcontrolpanel.views

import android.widget.SeekBar

class CompositeOnSeekBarChangeListener: SeekBar.OnSeekBarChangeListener {

    private var listeners: MutableList<SeekBar.OnSeekBarChangeListener> = ArrayList()

    fun addOnSeekBarChangeListener(listener: SeekBar.OnSeekBarChangeListener) {
        listeners.add(listener)
    }

    override fun onProgressChanged(seekBar: SeekBar?, position: Int, flag: Boolean) {
        for (listener in listeners) {
            listener.onProgressChanged(seekBar, position, flag)
        }
    }
    override fun onStartTrackingTouch(seekBar: SeekBar?) {
        for (listener in listeners) {
            listener.onStartTrackingTouch(seekBar)
        }
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        for (listener in listeners) {
            listener.onStopTrackingTouch(seekBar)
        }
    }
}