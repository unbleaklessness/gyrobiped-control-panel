package com.example.gyrobipedcontrolpanel.views

import android.content.Context
import android.graphics.Canvas
import android.view.View
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.AttributeSet
import com.example.gyrobipedcontrolpanel.mathematics.Matrix2
import com.example.gyrobipedcontrolpanel.mathematics.Vector2


class GyrobipedModel: View {

    // Leg angles:
    var leftThighAngle: Float = 0.0f
    var leftKneeAngle: Float = 0.0f
        set(value) { field = -value }
    var rightThighAngle: Float = 0.0f
        set(value) { field = -value }
    var rightKneeAngle: Float = 0.0f

    // Gyro angles:
    var frontGyroAngle: Float = 0.0f
        set(value) { field = -value }
    var rightGyroAngle: Float = 0.0f
        set(value) { field = -value }
    var backGyroAngle: Float = 0.0f
    var leftGyroAngle: Float = 0.0f

    // Leg velocities:
    var leftThighVelocity: Float = 0.0f
    var leftKneeVelocity: Float = 0.0f
    var rightThighVelocity: Float = 0.0f
    var rightKneeVelocity: Float = 0.0f

    // Gyro velocities:
    var frontGyroVelocity: Float = 0.0f
    var rightGyroVelocity: Float = 0.0f
    var backGyroVelocity: Float = 0.0f
    var leftGyroVelocity: Float = 0.0f

    // Gyroscope positions:
    var pitchPosition: Float = 0.0f
    var rollPosition: Float = 0.0f
    var yawPosition: Float = 0.0f

    // Gyroscope velocities:
    var pitchVelocity: Float = 0.0f
    var rollVelocity: Float = 0.0f
    var yawVelocity: Float = 0.0f

    // View painter:
    private val paint: Paint = Paint()

    // View center:
    private var center: Vector2 = Vector2()

    // Body:
    private var bodyCenter: Vector2 = Vector2()
    private var bodyWidth: Float = 0.0f
    private var bodyRound: Float = 0.0f
    private var bodyLeft: Float = 0.0f
    private var bodyTop: Float = 0.0f
    private var bodyRight: Float = 0.0f
    private var bodyBottom: Float = 0.0f

    // Gyros:
    private var gyrosTranslation: Vector2 = Vector2()
    private var gyrosHalfLength: Float = 0.0f
    private var gyrosLineWidth: Float = 0.0f
    private var gyroEdgesCircleRadius: Float = 0.0f
    private var gyroCentersCircleRadius: Float = 0.0f

    // Gyro edges:
    private var frontGyroStart: Vector2 = Vector2()
    private var frontGyroEnd: Vector2 = Vector2()
    private var rightGyroStart: Vector2 = Vector2()
    private var rightGyroEnd: Vector2 = Vector2()
    private var backGyroStart: Vector2 = Vector2()
    private var backGyroEnd: Vector2 = Vector2()
    private var leftGyroStart: Vector2 = Vector2()
    private var leftGyroEnd: Vector2 = Vector2()

    // Gyro centers:
    private var frontGyroCenter: Vector2 = Vector2()
    private var rightGyroCenter: Vector2 = Vector2()
    private var backGyroCenter: Vector2 = Vector2()
    private var leftGyroCenter: Vector2 = Vector2()

    // Legs:
    private var leftThigh: Vector2 = Vector2()
    private var rightThigh: Vector2 = Vector2()
    private var leftKnee: Vector2 = Vector2()
    private var rightKnee: Vector2 = Vector2()
    private var leftToe: Vector2 = Vector2()
    private var rightToe: Vector2 = Vector2()
    private var legsTranslation: Vector2 = Vector2()
    private var legsOffsetPlus: Vector2 = Vector2()
    private var legsOffsetMinus: Vector2 = Vector2()
    private var legsLineWidth: Float = 0.0f
    private var legsCircleRadius: Float = 0.0f
    private var legsLength: Float = 0.0f

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(
        context,
        attrs,
        defStyleAttr)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int): super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes)

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        center = Vector2(w / 2.0f, h / 2.0f)

        //
        // Body:
        //

        bodyWidth = w * 0.2f
        bodyRound = bodyWidth * 0.1f

        legsOffsetPlus = Vector2(bodyWidth * 0.35f, 0.0f)
        legsOffsetMinus = Vector2(-bodyWidth * 0.35f, 0.0f)

        bodyLeft = center.x - bodyWidth
        bodyTop = center.y - (bodyWidth * 2.0f)
        bodyRight = center.x + bodyWidth
        bodyBottom = center.y

        bodyCenter = Vector2((bodyLeft + bodyRight) / 2.0f, (bodyTop + bodyBottom) / 2.0f)

        //
        // Legs:
        //

        legsLength = w / 4.5f
        legsTranslation = Vector2(0.0f, legsLength)

        legsLineWidth = w * 0.03f
        legsCircleRadius = w * 0.03f

        //
        // Gyros:
        //

        gyrosHalfLength  = legsLength * 0.35f
        gyrosTranslation = Vector2(gyrosHalfLength, 0.0f)

        frontGyroCenter = Vector2(bodyCenter.x, (bodyTop + bodyCenter.y) / 2.0f)
        rightGyroCenter = Vector2((bodyRight + bodyCenter.x) / 2.0f, bodyCenter.y)
        backGyroCenter = Vector2(bodyCenter.x, (bodyBottom + bodyCenter.y) / 2.0f)
        leftGyroCenter = Vector2((bodyLeft + bodyCenter.x) / 2.0f, bodyCenter.y)

        gyrosLineWidth = w * 0.02f
        gyroEdgesCircleRadius = gyrosLineWidth / 2.0f
        gyroCentersCircleRadius = w * 0.025f
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.apply {

            //
            // Rotation matrices:
            //

            // Gyros:
            val frontGyroRotation = Matrix2.rotation(frontGyroAngle)
            val rightGyroRotation = Matrix2.rotation(rightGyroAngle)
            val backGyroRotation = Matrix2.rotation(backGyroAngle)
            val leftGyroRotation = Matrix2.rotation(leftGyroAngle)

            // Legs:
            val leftThighRotation = Matrix2.rotation(leftThighAngle)
            val rightThighRotation = Matrix2.rotation(rightThighAngle)
            val leftKneeRotation = rightThighRotation.multiply(Matrix2.rotation(leftKneeAngle))
            val rightKneeRotation = leftThighRotation.multiply(Matrix2.rotation(rightKneeAngle))

            //
            // Gyros:
            //

            frontGyroStart = frontGyroRotation.multiply(gyrosTranslation.negative()).add(frontGyroCenter)
            frontGyroEnd = frontGyroRotation.multiply(gyrosTranslation).add(frontGyroCenter)
            rightGyroStart = rightGyroRotation.multiply(gyrosTranslation.negative()).add(rightGyroCenter)
            rightGyroEnd = rightGyroRotation.multiply(gyrosTranslation).add(rightGyroCenter)
            backGyroStart = backGyroRotation.multiply(gyrosTranslation.negative()).add(backGyroCenter)
            backGyroEnd = backGyroRotation.multiply(gyrosTranslation).add(backGyroCenter)
            leftGyroStart = leftGyroRotation.multiply(gyrosTranslation.negative()).add(leftGyroCenter)
            leftGyroEnd = leftGyroRotation.multiply(gyrosTranslation).add(leftGyroCenter)

            //
            // Legs:
            //

            leftThigh = center.add(legsOffsetPlus)
            rightThigh = center.add(legsOffsetMinus)
            leftKnee = leftThighRotation.multiply(legsTranslation).add(leftThigh)
            rightKnee = rightThighRotation.multiply(legsTranslation).add(rightThigh)
            leftToe = leftKneeRotation.multiply(legsTranslation).add(leftKnee)
            rightToe = rightKneeRotation.multiply(legsTranslation).add(rightKnee)

            //
            // Drawing:
            //

            val defaultStrokeWidth = paint.strokeWidth
            paint.textSize = 20.0f
            paint.typeface = Typeface.create("Arial", Typeface.BOLD)

            // Body:
            paint.strokeWidth = defaultStrokeWidth
            paint.color = Color.parseColor("#AA7777")
            drawRoundRect(bodyLeft, bodyTop, bodyRight, bodyBottom, bodyRound, bodyRound, paint)

            // Gyro lines:
            paint.strokeWidth = gyrosLineWidth
            paint.color = Color.parseColor("#FFFF00")
            drawLine(frontGyroStart.x, frontGyroStart.y, frontGyroEnd.x, frontGyroEnd.y, paint)
            drawLine(rightGyroStart.x, rightGyroStart.y, rightGyroEnd.x, rightGyroEnd.y, paint)
            drawLine(backGyroStart.x, backGyroStart.y, backGyroEnd.x, backGyroEnd.y, paint)
            drawLine(leftGyroStart.x, leftGyroStart.y, leftGyroEnd.x, leftGyroEnd.y, paint)

            // Gyro centers:
            paint.strokeWidth = defaultStrokeWidth
            paint.color = Color.parseColor("#DD00DD")
            drawCircle(frontGyroCenter.x, frontGyroCenter.y, gyroCentersCircleRadius, paint)
            drawCircle(rightGyroCenter.x, rightGyroCenter.y, gyroCentersCircleRadius, paint)
            drawCircle(backGyroCenter.x, backGyroCenter.y, gyroCentersCircleRadius, paint)
            drawCircle(leftGyroCenter.x, leftGyroCenter.y, gyroCentersCircleRadius, paint)

            // Gyro servo numbers:
            paint.strokeWidth = defaultStrokeWidth
            paint.color = Color.parseColor("#000000")
            drawText("0", frontGyroCenter.x, frontGyroCenter.y - 20.0f, paint)
            drawText("1", backGyroCenter.x, backGyroCenter.y - 20.0f, paint)
            drawText("2", leftGyroCenter.x, leftGyroCenter.y - 20.0f, paint)
            drawText("3", rightGyroCenter.x, rightGyroCenter.y - 20.0f, paint)

            // Gyro edges:
            paint.strokeWidth = defaultStrokeWidth
            paint.color = Color.parseColor("#FFFF00")
            drawCircle(frontGyroStart.x, frontGyroStart.y, gyroEdgesCircleRadius, paint)
            drawCircle(frontGyroEnd.x, frontGyroEnd.y, gyroEdgesCircleRadius, paint)
            drawCircle(rightGyroStart.x, rightGyroStart.y, gyroEdgesCircleRadius, paint)
            drawCircle(rightGyroEnd.x, rightGyroEnd.y, gyroEdgesCircleRadius, paint)
            drawCircle(backGyroStart.x, backGyroStart.y, gyroEdgesCircleRadius, paint)
            drawCircle(backGyroEnd.x, backGyroEnd.y, gyroEdgesCircleRadius, paint)
            drawCircle(leftGyroStart.x, leftGyroStart.y, gyroEdgesCircleRadius, paint)
            drawCircle(leftGyroEnd.x, leftGyroEnd.y, gyroEdgesCircleRadius, paint)

            // Legs:
            paint.strokeWidth = legsLineWidth
            paint.color = Color.parseColor("#0000AA")
            drawLine(leftThigh.x, leftThigh.y, leftKnee.x, leftKnee.y, paint)
            drawLine(leftKnee.x, leftKnee.y, leftToe.x, leftToe.y, paint)
            paint.color = Color.parseColor("#AA0000")
            drawLine(rightThigh.x, rightThigh.y, rightKnee.x, rightKnee.y, paint)
            drawLine(rightKnee.x, rightKnee.y, rightToe.x, rightToe.y, paint)

            // Leg points:
            paint.strokeWidth = defaultStrokeWidth
            paint.color = Color.parseColor("#00FF00")
            drawCircle(leftThigh.x, leftThigh.y, legsCircleRadius, paint)
            drawCircle(rightThigh.x, rightThigh.y, legsCircleRadius, paint)
            drawCircle(leftKnee.x, leftKnee.y, legsCircleRadius, paint)
            drawCircle(rightKnee.x, rightKnee.y, legsCircleRadius, paint)
            drawCircle(leftToe.x, leftToe.y, legsCircleRadius, paint)
            drawCircle(rightToe.x, rightToe.y, legsCircleRadius, paint)

            // Leg servo numbers:
            paint.strokeWidth = defaultStrokeWidth
            paint.color = Color.parseColor("#000000")
            drawText("4", leftThigh.x + 20.0f, leftThigh.y, paint)
            drawText("5", rightThigh.x - 20.0f, rightThigh.y, paint)
            drawText("6", leftKnee.x + 20.0f, leftKnee.y, paint)
            drawText("7", rightKnee.x - 20.0f, rightKnee.y, paint)

        }
    }

    private fun animationStep() {
        frontGyroAngle += 0.02f
        rightGyroAngle += 0.04f
        backGyroAngle += 0.06f
        leftGyroAngle += 0.08f

        leftThighAngle += 0.02f
        rightThighAngle += 0.04f
        leftKneeAngle += 0.06f
        rightKneeAngle += 0.08f
    }

    fun gyrosInformation(): String {
        val builder = StringBuilder()
        val separator = "-".repeat(10)
        val headerFormat = "%-10s | %-10s | %-10s \n"
        val rowsFormat = "%-10s | %-10.5f | %-10.5f \n"
        builder.append(String.format(headerFormat, "Gyros", "Positions", "Velocities"))
        builder.append(String.format(headerFormat, separator, separator, separator))
        builder.append(String.format(rowsFormat, "Front", frontGyroAngle, frontGyroVelocity))
        builder.append(String.format(rowsFormat, "Back", backGyroAngle, backGyroVelocity))
        builder.append(String.format(rowsFormat, "Left", leftGyroAngle, leftGyroVelocity))
        builder.append(String.format(rowsFormat, "Right", rightGyroAngle, rightGyroVelocity))
        return builder.toString()
    }

    fun legsInformation(): String {
        val builder = StringBuilder()
        val separator = "-".repeat(11)
        val headerFormat = "%-11s | %-11s | %-11s \n"
        val rowsFormat = "%-11s | %-11.5f | %-11.5f \n"
        builder.append(String.format(headerFormat, "Legs", "Positions", "Velocities"))
        builder.append(String.format(headerFormat, separator, separator, separator))
        builder.append(String.format(rowsFormat, "Left Thigh", leftThighAngle, leftThighVelocity))
        builder.append(String.format(rowsFormat, "Left Knee", leftKneeAngle, leftKneeVelocity))
        builder.append(String.format(rowsFormat, "Right Thigh", rightThighAngle, rightThighVelocity))
        builder.append(String.format(rowsFormat, "Right Knee", rightKneeAngle, rightKneeVelocity))
        return builder.toString()
    }

    fun gyroscopeInformation(): String {
        val builder = StringBuilder()
        val separator = "-".repeat(11)
        val headerFormat = "%-11s | %-11s | %-11s \n"
        val rowsFormat = "%-11s | %-11.5f | %-11.5f \n"
        builder.append(String.format(headerFormat, "GL", "Positions", "Velocities"))
        builder.append(String.format(headerFormat, separator, separator, separator))
        builder.append(String.format(rowsFormat, "Roll", rollPosition, rollVelocity))
        builder.append(String.format(rowsFormat, "Pitch", pitchPosition, pitchVelocity))
        builder.append(String.format(rowsFormat, "Yaw", yawPosition, yawVelocity))
        return builder.toString()
    }
}