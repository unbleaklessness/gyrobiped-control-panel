package com.example.gyrobipedcontrolpanel.views

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageButton
import com.example.gyrobipedcontrolpanel.stateful.StatefulElement

class MyImageButton: ImageButton, StatefulElement {

    private var state: Byte = 0

    private var compositeOnClickListener: CompositeOnClickListener =
        CompositeOnClickListener()

    init {
        setOnClickListener {
            val okState: Byte = 1
            state = if (state == okState) 0 else 1
        }
    }

    constructor(context: Context?): super(context)
    constructor(context: Context?, attrs: AttributeSet?): super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr)

    override fun getState(): Byte {
        return state
    }

    override fun setOnClickListener(listener: OnClickListener?) {

        listener?.let {
            compositeOnClickListener.addOnClickListener(it)
        }

        super.setOnClickListener(compositeOnClickListener)
    }

    fun toggleOff() {
        state = 0
    }
}