package com.example.gyrobipedcontrolpanel.views

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.util.AttributeSet
import android.widget.SeekBar
import com.example.gyrobipedcontrolpanel.stateful.StatefulElement

class MySeekBar: SeekBar, StatefulElement {

    init {
        setOnSeekBarChangeListener(object: OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, position: Int, flag: Boolean) {
                state = position.toByte()
            }
            override fun onStartTrackingTouch(p0: SeekBar?) {}
            override fun onStopTrackingTouch(p0: SeekBar?) {}
        })
    }

    private var state: Byte = 0

//    private var onSeekBarChangeListener: CompositeOnSeekBarChangeListener = CompositeOnSeekBarChangeListener()

    constructor(context: Context): super(context)
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(
        context,
        attrs,
        defStyleAttr)
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int): super(
        context,
        attrs,
        defStyleAttr,
        defStyleRes)

    override fun getState(): Byte {
        return state
    }

//    override fun setOnSeekBarChangeListener(listener: OnSeekBarChangeListener?) {
//
//        listener?.let {
//            onSeekBarChangeListener.addOnSeekBarChangeListener(it)
//        }
//
//        super.setOnSeekBarChangeListener(listener)
//    }
}