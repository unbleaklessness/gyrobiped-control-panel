package com.example.gyrobipedcontrolpanel.views

import android.content.Context
import android.content.res.ColorStateList
import android.support.design.button.MaterialButton
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import com.example.gyrobipedcontrolpanel.R
import com.example.gyrobipedcontrolpanel.stateful.StatefulElement

class ToggleButton: MaterialButton, StatefulElement {

    private var isActive: Boolean = false
        set(value) {
            field = value
            backgroundTintList = getToggleColor()
        }

    private var activeColor: ColorStateList = ColorStateList.valueOf(ContextCompat.getColor(context,
        R.color.toggle_button_active_color
    ))

    private var inactiveColor: ColorStateList = ColorStateList.valueOf(ContextCompat.getColor(context,
        R.color.toggle_button_inactive_color
    ))

    private var compositeOnClickListener: CompositeOnClickListener? =
        CompositeOnClickListener()

    init {
        backgroundTintList = getToggleColor()

        setOnClickListener {
            toggle()
        }
    }

    constructor(context: Context?): super(context)
    constructor(context: Context?, attrs: AttributeSet?): super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr)

    override fun getState(): Byte {
        return if (isActive) {
            1
        } else {
            0
        }
    }

    override fun setOnClickListener(listener: OnClickListener?) {

        listener?.let {
            compositeOnClickListener?.addOnClickListener(it)
        }

        super.setOnClickListener(compositeOnClickListener)
    }

    private fun toggle() {
        isActive = !isActive
    }

    fun toggleOff() {
        isActive = false
    }

    private fun getToggleColor(): ColorStateList {
        return if (isActive) {
            activeColor
        } else {
            inactiveColor
        }
    }
}